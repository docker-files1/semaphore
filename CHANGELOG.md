# [1.4.0](/Docker_Files/semaphore/compare/1.4.0...master)
* CICD migraton from Jenkins to GitlabCI

# [1.3.0](/Docker_Files/semaphore/compare/1.3.0...master)
* Docker linter apply and little hotfix that might affect to some files that match with the current `.gitignore`

# [1.2.0](/Docker_Files/semaphore/compare/1.2.0...master)
* Se actualiza el formato de la documentación

# [1.1.0](/Docker_Files/semaphore/compare/1.1.0...master)
* Se agrega el paquetersync a fin de poder hacer uso del modulo de ansible `synchronize`

# [1.0.0](/Docker_Files/semaphore/compare/1.0.0...master)
* Primera versión del docker file `semaphore` con la librería **jmespath**
