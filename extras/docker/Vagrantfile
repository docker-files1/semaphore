# -*- mode: ruby -*-
# vi: set ft=ruby :
#
# Remember export the variables with your docker credentials before to run vagrant up, by example:
#
# export DOCKERUSER="username"
# export DOCKERPASSWORD="password"

Vagrant.configure("2") do |config|

  config.vm.box = "debian/buster64"

  config.vm.network "forwarded_port", guest: 3000, host: 3000, host_ip: "127.0.0.1", id: "ftp"

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = false

    # Customize the amount of memory on the VM:
    vb.memory = "2048"

    # Customize configurations on the VM:
    vb.customize ["modifyvm", :id, "--vram", "128"]
    vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
  end

  config.vm.synced_folder "../../", "/vagrant", disabled: false

  # Enable provisioning with a shell script.
  # Install and prepare docker for create the docker image
  config.vm.provision "shell", inline: <<-'SHELL'
    apt remove -y docker docker-engine docker.io containerd runc
    apt update -y && apt install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common vim
    curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
    apt update -y && apt install -y docker-ce docker-ce-cli containerd.io
    mkdir /root/.docker && echo -e "{\\n\\t\"experimental\": \"enabled\"\\n}" > /root/.docker/config.json
    echo -e "\\nPreparing docker compose..."
    curl -L "https://github.com/docker/compose/releases/download/1.28.4/docker-compose-$(uname -s)-$(uname -m)" -o /bin/docker-compose
    chmod +x /bin/docker-compose && docker-compose --version
    echo -e "\\nPreparing qemu for use with docker..."
    docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
    echo -e "\\nBuilder creation and set ad default..."
    docker buildx create --name mybuilder --driver docker-container --use
    echo -e "\\nInspect previus builder configured..."
    docker buildx inspect --bootstrap
    echo -e "\\nVirtual machine is ready!"
  SHELL

  # Enable provisioning with a shell script.
  # Docker login to docker hub
  # config.vm.provision "shell", inline: <<-SHELL
  #   echo "#{ENV['DOCKERPASSWORD']}" | sudo docker login --username "#{ENV['DOCKERUSER']}" --password-stdin
  # SHELL

end
