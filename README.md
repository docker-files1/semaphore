[![pipeline status](https://gitlab.com/docker-files1/semaphore/badges/development/pipeline.svg)](https://gitlab.com/docker-files1/semaphore/-/commits/development) ![Project version](https://img.shields.io/docker/v/oscarenzo/semaphore?sort=date) ![Docker image pulls](https://img.shields.io/docker/pulls/oscarenzo/semaphore) ![Docker image size](https://img.shields.io/docker/image-size/oscarenzo/semaphore?sort=date) ![Project license](https://img.shields.io/gitlab/license/docker-files1/semaphore)

# semaphore
Ansible Semaphore is a modern UI for Ansible. It lets you easily run Ansible playbooks, get notifications about fails, control access to deployment system; full documentation is available on:

## 🧾 Components
 - Debian
 - Semaphore
 - py3-jmespath
 - rsync

## 🔗 References
https://github.com/ansible-semaphore/semaphore

## ✍️ Advices
Use this docker image if you need to use features that needs this packages:
 - py3-jmespath
 - rsync

## 💬 Legend
* NKS => No key sensitive
* KS => Key sensitive
