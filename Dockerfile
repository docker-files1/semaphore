FROM semaphoreui/semaphore:v2.8.77

LABEL DistBase="Alpine 3.16.3"

USER root
RUN apk add --no-cache py3-jmespath rsync
USER semaphore

# docker build -t oscarenzo/semaphore:latest .
# docker buildx build --push -t oscarenzo/semaphore:latest --platform linux/amd64,linux/arm/v7 .
